scene2 = `{
	"hfov": 110,
    "yaw": 5,
    "type": "equirectangular",
    "panorama": "https://actualquak.github.io/School-View/360_1.jpg",
    "hotSpots": [{
    "pitch": -0.6,
        "yaw": 37.1,
        "type": "scene",
        "text": "Room",
        "sceneId": "room1",
        "targetYaw": -23,
        "targetPitch": 2
	}]
}`;
scene2json = JSON.parse(scene2)