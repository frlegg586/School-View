scene1 = `{
    "hfov": 110,
    "pitch": -3,
    "yaw": 117,
    "type": "equirectangular",
    "panorama": "https://actualquak.github.io/School-View/room1.jpg",
    "hotSpots": [{
		"pitch": -2.1,
        "yaw": 132.9,
        "type": "scene",
        "text": "Airport",
        "sceneId": "room2"
    }]
}`;
scene1json = JSON.parse(scene1)